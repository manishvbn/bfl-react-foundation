import * as React from 'react';

import './ComponentOne.css';

export class ComponentOne extends React.Component {
    public render() {
        return (
            <>
                <h1 className='text-success'>Hello from Component One</h1>
                <h1 className='card1'>From Component One</h1>
            </>
        );
    }
}
