import * as React from 'react';

import './ComponentTwo.css';

export class ComponentTwo extends React.Component {
    public render() {
        return (
            <>
                <h1 className='text-primary'>Hello from Component Two</h1>
                <h1 className='card2'>From Component Two</h1>
            </>
        );
    }
}
