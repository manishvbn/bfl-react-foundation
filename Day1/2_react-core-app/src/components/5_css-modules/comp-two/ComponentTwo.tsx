import * as React from 'react';

import classes from './ComponentTwo.module.css';

export class ComponentTwo extends React.Component {
    public render() {
        return (
            <>
                <h1 className='text-primary'>Hello from Component Two</h1>
                <h1 className={classes.card}>From Component Two</h1>
            </>
        );
    }
}
