import * as React from 'react';

import classes from './ComponentOne.module.css';

export class ComponentOne extends React.Component {
    public render() {
        return (
            <>
                <h1 className='text-success'>Hello from Component One</h1>
                <h1 className={classes.card}>From Component One</h1>
            </>
        );
    }
}
