import * as React from 'react';

export interface IComponentWithPropsProps {
    id: number;
    name: string;
    address: {
        state: string;
    },
    display: () => void;
}

export interface IComponentWithPropsState {
    name: string;
}

export default class ComponentWithProps extends React.Component<IComponentWithPropsProps, IComponentWithPropsState> {
    constructor(props: IComponentWithPropsProps) {
        super(props);

        // Shallow Copy
        // this.state = { ...this.props };
        // this.state.name = "Abhijeet";

        this.state = JSON.parse(JSON.stringify(this.props));
        this.state.name = "Abhijeet";

        // console.log("Ctor: ", this.props);
    }

    public componentDidMount() {
        this.setState({ name: "Abhijeet" });
        // console.log("CDM: ", this.props);
    }

    public render() {
        console.log("Render: ", this.props);
        console.log("Render: ", this.state);
        
        return (
            <div>

            </div>
        );
    }
}
