import * as React from 'react';
import { ComponentWithStateType } from '../../types/ComponentWithStateType';

interface IComponentWithStateProps { 

}

export class ComponentWithState extends React.Component<IComponentWithStateProps, ComponentWithStateType> {
    private message: string;

    constructor(p: IComponentWithStateProps) {
        super(p);

        // State must be initialized in the constructor
        // State must be set to an object or null
        // this.state = "India";

        this.state = { country: "India" };
        this.message = "Hello";
        console.log("Ctor: ", this.state);
    }

    public render() {
        console.log("Render: ", this.state);

        return (
            <>
                <h2 className="text-primary">Component With State</h2>
                <h2 className="text-primary">Country, {this.state.country}</h2>
                <h2 className="text-primary">Message, {this.message}</h2>
            </>
        );
    }
}
