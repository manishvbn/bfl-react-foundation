import * as React from 'react';

export class ComponentOne extends React.Component {
    public render() {
        return (
            <h1 className='text-success'>Hello from Component One</h1>
        );
    }
}
