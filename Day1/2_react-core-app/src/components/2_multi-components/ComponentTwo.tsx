import * as React from 'react';

export class ComponentTwo extends React.Component {
    public render() {
        return (
            <h1 className='text-primary'>Hello from Component Two</h1>
        );
    }
}
