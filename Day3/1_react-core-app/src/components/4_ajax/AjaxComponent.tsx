import * as React from 'react';
import { postApiClient } from '../../services/post-api-client';
import LoaderAnimation from '../common/LoaderAnimation';
import DataTable, { Item } from '../common/DataTable';
import { Post } from './post-interface';

const convertToItems = (posts: Post[]): Item[] => {
    return posts.map((post) => ({
        id: post.id,
        title: post.title,
        body: post.body,
    }));
};

interface IAjaxComponentProps {
}

const AjaxComponent: React.FunctionComponent<IAjaxComponentProps> = (props) => {
    const [cState, setcState] = React.useState<{ posts: Post[]; message: string; flag: boolean }>({
        posts: [],
        message: "Loading Data, please wait...",
        flag: false
    });

    // React.useEffect(() => {
    //     postApiClient.getAllPosts().then((posts) => {
    //         setcState({ posts: posts, message: "", flag: true });
    //     }).catch((eMsg: string) => {
    //         setcState({ posts: [], message: eMsg, flag: true });
    //     });
    // }, []);

    React.useEffect(() => {
        const fetch = async () => {
            try {
                const data: Post[] = await postApiClient.getAllPosts();
                setcState({ posts: data, message: "", flag: true });
            }
            catch (error: any) {
                setcState({ posts: [], message: error.message, flag: true });
            }
        };

        fetch();
    }, []);

    return <>
        <div className="row mt-5">
            <h4 className="text-warning text-center text-uppercase font-weight-bold">{cState.message}</h4>
        </div>

        {cState.flag ? null : <LoaderAnimation />}

        <DataTable items={convertToItems(cState.posts)}>
            <h4 className="text-primary text-uppercase font-weight-bold">Posts Table</h4>
        </DataTable>
    </>;
};

export default AjaxComponent;
