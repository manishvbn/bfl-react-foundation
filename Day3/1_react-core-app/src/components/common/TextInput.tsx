import React, { ChangeEvent, useEffect, useState } from 'react';

interface ITextInputProps {
    name: string;
    label: string;
    placeholder?: string;
    value: any;
    readOnly?: boolean;
    onChange?: (event: ChangeEvent<HTMLInputElement>) => void;
    validate?: (value: string) => string | null;
    isFormSubmitted?: boolean;
}

const TextInput: React.FC<ITextInputProps> = ({
    name,
    label,
    placeholder,
    value,
    readOnly,
    onChange,
    validate,
    isFormSubmitted = false
}) => {
    const [error, setError] = useState<string | null>(null);

    const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
        const inputValue = e.target.value;
        if (onChange) {
            onChange(e);

            if (validate && isFormSubmitted) {
                const errorMessage = validate(inputValue);
                setError(errorMessage);
            }
        }
    };

    useEffect(() => {
        if (validate && isFormSubmitted) {
            const errorMessage = validate(value);
            setError(errorMessage);
        }
    }, [value, validate, isFormSubmitted])

    return (
        <div className={`form-group mb-1 ${error ? 'has-error' : ''}`}>
            <label className="mb-0" htmlFor={name}>
                {label}
            </label>
            <input
                type="text"
                className={`form-control ${error ? 'is-invalid' : ''}`}
                id={name}
                name={name}
                placeholder={placeholder}
                value={value}
                readOnly={readOnly}
                onChange={handleChange}
            />
            {error && <div className='invalid-feedback'>{error}</div>}
        </div>
    );
}

export default TextInput;
