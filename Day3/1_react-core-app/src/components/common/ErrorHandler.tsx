import * as React from 'react';
import { ReactNode } from 'react';

export interface IErrorHandlerProps {
    children: ReactNode;
}

export interface IErrorHandlerState {
    hasError: boolean;
}

export default class ErrorHandler extends React.Component<IErrorHandlerProps, IErrorHandlerState> {
    constructor(props: IErrorHandlerProps) {
        super(props);

        this.state = {
            hasError: false
        };
    }

    public render() {
        if (this.state.hasError) {
            const errImg = require('../../assets/error.png');
            return (
                <div className="text-center mt-5">
                    <h3 className="text-danger mb-5">Something went wrong, please conatct admin!</h3>
                    <img src={errImg} alt="Error" className='img-thumbnail' />
                </div>
            );
        } else {
            return this.props.children;
        }
    }

    componentDidCatch(error: Error, info: React.ErrorInfo): void {
        // You can log the error to an error reporting service here
        console.log("CDC, Error: ", error);
        console.log("CDC, Info: ", info);
    }

    static getDerivedStateFromError(error: Error): IErrorHandlerState { 
        // Update state so that the next render will show the fallback UI.
        return { hasError: true };
    }
}