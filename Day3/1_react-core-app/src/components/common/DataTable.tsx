import React, { MouseEvent } from 'react';

export interface Item {
    [key: string]: string | number;
}

interface DataTableProps {
    items: Item[];
    children?: React.ReactNode;
    onSelect?: (item: Item, isEdit: boolean) => void;
    onDelete?: (id: number) => void;
}

const Th: React.FC<{
    item: Item;
}> = ({ item }) => {
    const allHeads = [...Object.keys(item), '', 'actions', ''];

    const ths = allHeads.map((head, index) => {
        return <th key={index}>{(head === '' ? '' : head.toUpperCase())}</th>;
    });

    return <tr>{ths}</tr>;
}

const Tr: React.FC<{
    item: Item;
    onSelect?: (item: Item, isEdit: boolean) => void;
    onDelete?: (id: number) => void;
}> = ({ item, onSelect, onDelete }) => {
    const allValues = [...Object.values(item),
    <a href="/#" className="text-primary" onClick={(e: MouseEvent<HTMLAnchorElement>) => {
        e.preventDefault();
        if (onSelect) onSelect(item, false);
    }}>Details</a>,
    <a href="/#" className="text-warning" onClick={(e: MouseEvent<HTMLAnchorElement>) => {
        e.preventDefault();
        if (onSelect) onSelect(item, true);
    }}>Edit</a>,
    <a href="/#" className="text-danger" onClick={(e: MouseEvent<HTMLAnchorElement>) => {
        e.preventDefault();
        if (onDelete) onDelete(Number(item.id));
    }}>Delete</a>];

    const tds = allValues.map((item, index) => {
        return <td key={index}>{item}</td>;
    });

    return <tr>{tds}</tr>;
};

const DataTable: React.FC<DataTableProps> = ({ items, children, onSelect, onDelete }) => {
    let headers: JSX.Element | null = null;
    let trs: JSX.Element[] | null = null;

    if (items && items.length) {
        const [item] = items;
        headers = <Th item={item} />;
        trs = items.map((item, index) => {
            return <Tr item={item} key={index} onSelect={onSelect} onDelete={onDelete} />;
        });
    }
    
    return (
        <>
            {children && children}
            <table className="table table-striped">
                <thead>{headers}</thead>
                <tbody>{trs}</tbody>
            </table>
        </>
    );
}

export default DataTable;