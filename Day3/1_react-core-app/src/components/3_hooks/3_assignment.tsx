import * as React from 'react';

export interface ICounterProps {
    interval?: number
}

const Counter: React.FC<ICounterProps> = ({interval = 1}) => {
    const [flag, setFlag] = React.useState<boolean>(false);
    const [count, setCount] = React.useState<number>(0);
    const clickCount = React.useRef<number>(0);

    const manageClickCount = (): void => {
        clickCount.current += 1;
        if (clickCount.current > 9) {
            setFlag(true);
        }
    }

    const inc = (): void => {
        setCount(count + interval);
        manageClickCount();
    };

    const dec = (): void => {
        setCount(count - interval);
        manageClickCount();
    };

    const reset = (): void => {
        clickCount.current = 0;
        setCount(0);
        setFlag(false);
    };

    return (
        <>
            <div className="text-center">
                <h3 className="text-info">Counter Component</h3>
            </div>
            <div className="d-grid gap-2 mx-auto col-6">
                <input type="text" className="form-control form-control-lg" value={count} readOnly />
                <button className="btn btn-info" disabled={flag} onClick={inc}>
                    <span className='fs-4'>+</span>
                </button>
                <button className="btn btn-info" disabled={flag} onClick={dec}>
                    <span className='fs-4'>-</span>
                </button>
                <button className="btn btn-secondary" disabled={!flag} onClick={reset}>
                    <span className='fs-4'>Reset</span>
                </button>
            </div>
        </>
    );
}

const Assignment: React.FunctionComponent<{}> = (props) => {
    return <>
        <Counter />
        <hr />
        <Counter interval={10} />
    </>;
};

export default Assignment;
