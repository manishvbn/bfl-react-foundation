import React, { useContext } from "react";

interface CounterState {
    count: number;
}

interface CounterAction {
    type: string;
    payload: number;
}

const counterState: CounterState = { count: 0 };

const CounterActions = {
    increment: 'increment',
    decrement: 'decrement'
}

const counterReducer = (state: CounterState, action: CounterAction): CounterState => {
    switch (action.type) {
        case CounterActions.increment:
            return { count: state.count + action.payload };
        case CounterActions.decrement:
            return { count: state.count - action.payload };
        default:
            return state;
    }
}

const Counter: React.FC = () => {
    // const context = useContext(CounterContext);
    const context = useCounter();
    
    return (
        <>
            <div className="text-center">
                <h1 className="text-primary">Counter Component</h1>
            </div>
            <div className="d-grid gap-2 mx-auto col-6">
                <div className="text-center">
                    <h2 className="text-primary">{context?.counterState.count}</h2>
                </div>
                <button className="btn btn-primary" onClick={() => context?.counterDispatch({ type: CounterActions.increment, payload: 1 })}>
                    <span className='fs-4'>+</span>
                </button>
                <button className="btn btn-primary" onClick={() => context?.counterDispatch({ type: CounterActions.decrement, payload: 1 })}>
                    <span className='fs-4'>-</span>
                </button>
            </div>
        </>
    );
}

const CounterSibling: React.FC = () => {
    // const context = useContext(CounterContext);
    const context = useCounter();

    return (
        <>
            <div className="text-center">
                <h1 className="text-warning">Counter Sibling Component</h1>
            </div>
            <div className="d-grid gap-2 mx-auto col-6">
                <div className="text-center">
                    <h2 className="text-warning">{context?.counterState.count}</h2>
                </div>
                <button className="btn btn-warning" onClick={() => context?.counterDispatch({ type: CounterActions.increment, payload: 1 })}>
                    <span className='fs-4'>+</span>
                </button>
                <button className="btn btn-warning" onClick={() => context?.counterDispatch({ type: CounterActions.decrement, payload: 1 })}>
                    <span className='fs-4'>-</span>
                </button>
            </div>
        </>
    );
}

function useCounter() {
    const context = useContext(CounterContext);

    if(!context) {
        throw new Error('useCounter must be used within CounterProvider');
    }

    return context;
}

const CounterContext = React.createContext<{ counterState: CounterState, counterDispatch: React.Dispatch<CounterAction> } | undefined>(undefined);

const ContextHookDemo: React.FC = () => {
    const [state, dispatch] = React.useReducer(counterReducer, counterState);

    return (
        <CounterContext.Provider value={{ counterState: state, counterDispatch: dispatch }}>
            <Counter />
            <hr />
            <CounterSibling />
        </CounterContext.Provider>
    );
};

export default ContextHookDemo;