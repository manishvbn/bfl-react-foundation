import React from "react";

interface CounterState {
    count: number;
}

interface CounterAction {
    type: string;
    payload: number;
}

const counterState: CounterState = { count: 0 };

const CounterActions = {
    increment: 'increment',
    decrement: 'decrement'
}

const counterReducer = (state: CounterState, action: CounterAction): CounterState => {
    switch (action.type) {
        case CounterActions.increment:
            return { count: state.count + action.payload };
        case CounterActions.decrement:
            return { count: state.count - action.payload };
        default:
            return state;
    }
}

const Counter: React.FC = () => {
    const [state, dispatch] = React.useReducer(counterReducer, counterState);

    return (
        <>
            <div className="text-center">
                <h1 className="text-primary">Counter Component</h1>
            </div>
            <div className="d-grid gap-2 mx-auto col-6">
                <div className="text-center">
                    <h2 className="text-primary">{state.count}</h2>
                </div>
                <button className="btn btn-primary" onClick={() => dispatch({ type: CounterActions.increment, payload: 1 })}>
                    <span className='fs-4'>+</span>
                </button>
                <button className="btn btn-primary" onClick={() => dispatch({ type: CounterActions.decrement, payload: 1 })}>
                    <span className='fs-4'>-</span>
                </button>
            </div>
        </>
    );
}

const CounterSibling: React.FC = () => {
    const [state, dispatch] = React.useReducer(counterReducer, counterState);

    return (
        <>
            <div className="text-center">
                <h1 className="text-warning">Counter Sibling Component</h1>
            </div>
            <div className="d-grid gap-2 mx-auto col-6">
                <div className="text-center">
                    <h2 className="text-warning">{state.count}</h2>
                </div>
                <button className="btn btn-warning" onClick={() => dispatch({ type: CounterActions.increment, payload: 1 })}>
                    <span className='fs-4'>+</span>
                </button>
                <button className="btn btn-warning" onClick={() => dispatch({ type: CounterActions.decrement, payload: 1 })}>
                    <span className='fs-4'>-</span>
                </button>
            </div>
        </>
    );
}

const ReducerHookDemo: React.FC = () => (
    <>
        <Counter />
        <hr />
        <CounterSibling />
    </>
);

export default ReducerHookDemo;