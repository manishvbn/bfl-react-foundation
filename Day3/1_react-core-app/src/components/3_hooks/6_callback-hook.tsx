import React, { ReactNode } from 'react';

const Counter: React.FC<{ handleClick: () => void, children: ReactNode }> = React.memo(({ handleClick, children }) => {
    console.log("Counter Rendered: ", children);

    return (
        <button className='btn btn-primary' onClick={handleClick}>
            {children}
        </button>
    );
});

const CallbackHookDemo: React.FC = () => {
    console.log("CallbackHookDemo Rendered");
    const [incCount, setIncCount] = React.useState(0);
    const [decCount, setDecCount] = React.useState(0);

    // const increment = () => { setIncCount(incCount + 1) };
    // const decrement = () => { setDecCount(decCount - 1) };

    const increment = React.useCallback(() => { setIncCount(incCount + 1) }, [incCount]);
    const decrement = React.useCallback(() => { setDecCount(decCount - 1) }, [decCount]);

    return (
        <div className="text-center mt-5">
            <h2 className="text-primary">
                {incCount}
            </h2>
            <Counter handleClick={increment}>
                Incrementer
            </Counter>

            <h2 className="text-primary">
                {decCount}
            </h2>
            <Counter handleClick={decrement}>
                Decrementer
            </Counter>
        </div>
    );
}

export default CallbackHookDemo;