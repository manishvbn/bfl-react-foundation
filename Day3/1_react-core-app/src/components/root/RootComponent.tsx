/* eslint-disable */
import React from 'react';
import CrudAssignment from '../1_crud-assignment/CrudAssignment';
import PortalRoot from '../2_portals/PortalRoot';
import ErrorHandler from '../common/ErrorHandler';
import StateHookDemo from '../3_hooks/1_state-hook';
import EffectHookDemo from '../3_hooks/2_effect-hook';
import Assignment from '../3_hooks/3_assignment';
import ReducerHookDemo from '../3_hooks/4_reducer-hook';
import ContextHookDemo from '../3_hooks/5_context-hook';
import CallbackHookDemo from '../3_hooks/6_callback-hook';
import AjaxComponent from '../4_ajax/AjaxComponent';

const RootComponent: React.FC = () => {
    return (
        <div className='container'>
            <ErrorHandler>
                {/* <CrudAssignment /> */}
                {/* <PortalRoot /> */}

                {/* <StateHookDemo /> */}
                {/* <EffectHookDemo /> */}
                {/* <Assignment /> */}
                {/* <ReducerHookDemo /> */}
                {/* <ContextHookDemo /> */}
                {/* <CallbackHookDemo /> */}
                <AjaxComponent />
            </ErrorHandler>
        </div>
    );
};

export default RootComponent;