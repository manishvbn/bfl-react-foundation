import React from 'react';

interface WithoutPortalProps {
    message: string;
    isOpen: boolean;
    onClose: () => void;
}

const WithoutPortal: React.FC<WithoutPortalProps> = ({ message, isOpen, onClose }) => {
    if (!isOpen) return null;

    return (
        <div className="app-modal">
            <h2>{message}</h2>
            <button onClick={onClose}>Close</button>
        </div>
    );
};

export default WithoutPortal;
