import * as React from 'react';
import './PortalRoot.css';
import WithoutPortal from './WithoutPortal';
import WithPortal from './WithPortal';

export interface IPortalRootProps {
}

export interface IPortalRootState {
    mflag: boolean;
    pflag: boolean;
}

export default class PortalRoot extends React.Component<IPortalRootProps, IPortalRootState> {
    constructor(props: IPortalRootProps) {
        super(props);

        this.state = {
            mflag: false,
            pflag: false
        };
    }

    setmFlag(f: boolean) {
        this.setState({ mflag: f });
    }

    setpFlag(f: boolean) {
        this.setState({ pflag: f });
    }

    public render() {
        return (
            <div className="app-container">
                <div className="button-container">
                    <button className="button" onClick={() => this.setmFlag(true)}>
                        Without Portal
                    </button>

                    <button className="button" onClick={() => this.setpFlag(true)}>
                        With Portal
                    </button>
                </div>

                <WithoutPortal message="Hello, I am a Modal Window" isOpen={this.state.mflag}
                    onClose={() => this.setmFlag(false)} />

                <WithPortal message="Hello, I am a Modal Window" isOpen={this.state.pflag}
                    onClose={() => this.setpFlag(false)} />
            </div>
        );
    }
}
