import React from 'react';
import ReactDOM from 'react-dom';

interface WithPortalProps {
    message: string;
    isOpen: boolean;
    onClose: () => void;
}

const WithPortal: React.FC<WithPortalProps> = ({ message, isOpen, onClose }) => {
    if (!isOpen) return null;

    return ReactDOM.createPortal(
        <div className="app-modal">
            <h2>{message}</h2>
            <button onClick={onClose}>Close</button>
        </div>, document.body
    );
};

export default WithPortal;
