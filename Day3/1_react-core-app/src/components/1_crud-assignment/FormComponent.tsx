import { ChangeEvent, useState } from "react";
import TextInput from "../common/TextInput";
import { Employee } from "./Employee";

const FormComponent: React.FC<{
    employee: Employee,
    changeEmployee: (e: ChangeEvent<HTMLInputElement>) => void,
    saveEmployee: () => void
}> = (props) => {
    const [isFormSubmitted, setFormSubmitted] = useState(false);

    const validateEmployeeName = (value: string) => {
        if (!value) {
            return 'Employee Name is required';
        }
        return null;
    }

    const validateDesignation = (value: string) => {
        if (value.length < 3) {
            return 'Designation must be at least 3 characters';
        }
        return null;
    }

    const handleSubmit = (e: React.FormEvent) => {
        e.preventDefault();
        setFormSubmitted(true);
        const { employee } = props;

        const nameError = validateEmployeeName(employee.name);
        const designationError = validateDesignation(employee.designation);

        if (nameError || designationError) {
            return;
        }

        props.saveEmployee();
        setFormSubmitted(false);
    };

    return (
        <>
            <div className='row'>
                <div className='col-sm-6 offset-sm-3'>
                    <form className='form-horizontal' autoComplete='off' onSubmit={handleSubmit}>
                        <fieldset>
                            <legend className="text-center text-secondary text-uppercase font-weight-bold">Add/Edit Employee Information</legend>
                            <hr className="mt-0" />
                            <TextInput
                                label={"Employee ID"}
                                name={"id"}
                                value={props.employee.id.toString()}
                                readOnly={true} />
                            <TextInput
                                label={"Employee Name"}
                                name={"name"}
                                value={props.employee.name}
                                onChange={props.changeEmployee}
                                validate={validateEmployeeName}
                                isFormSubmitted={isFormSubmitted} />
                            <TextInput
                                label={"Designation"}
                                name={"designation"}
                                value={props.employee.designation}
                                onChange={props.changeEmployee}
                                validate={validateDesignation}
                                isFormSubmitted={isFormSubmitted} />
                            <TextInput
                                label={"Salary"}
                                name={"salary"}
                                value={props.employee.salary}
                                onChange={props.changeEmployee} />

                            <div className="d-grid gap-2 mx-auto mt-3">
                                <button type="submit" className="btn btn-success">Save</button>
                                <button type="reset" className="btn btn-primary">Reset</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </>
    );
};

export default FormComponent;