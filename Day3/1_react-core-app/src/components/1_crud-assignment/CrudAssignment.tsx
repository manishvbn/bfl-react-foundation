import React, { ChangeEvent, Component } from 'react';
import DataTable, { Item } from '../common/DataTable';
import ConfirmModal from '../common/ConfirmModal';
import { Employee } from './Employee';
import FormComponent from './FormComponent';
import { convertEmployeesToItems, convertItemToEmployee } from './converters';

interface CrudAssignmentState {
    employees: Employee[];
    employee: Employee;
    edit: boolean;
    showDeleteConfirmModal: boolean;
    employeeToDeleteId: number;
}

interface CrudAssignmentProps { }

class CrudAssignment extends Component<CrudAssignmentProps, CrudAssignmentState> {
    constructor(props: {}) {
        super(props);
        this.state = {
            employees: [],
            employee: { id: 1, name: "", designation: "", salary: "" },
            edit: false,
            showDeleteConfirmModal: false,
            employeeToDeleteId: 0
        };
        this.changeEmployee = this.changeEmployee.bind(this);
        this.saveEmployee = this.saveEmployee.bind(this);
        this.selectEmployee = this.selectEmployee.bind(this);
        this.hideDeleteConfirmModal = this.hideDeleteConfirmModal.bind(this);
        this.confirmDelete = this.confirmDelete.bind(this);
    }

    changeEmployee(e: ChangeEvent<HTMLInputElement>) {
        const field: keyof Employee = e.target.id as keyof Employee;
        const value = e.target.value;

        const newEmployee: Employee = { ...this.state.employee };
        if (field === 'id') {
            newEmployee[field] = parseInt(value);
        } else {
            newEmployee[field] = value;
        }

        this.setState({ employee: newEmployee });
    }

    saveEmployee() {
        if (this.state.edit) {
            const temp_employees = [...this.state.employees];
            const itemIndex = temp_employees.findIndex((item) => item.id === this.state.employee.id);
            temp_employees.splice(itemIndex, 1, { ...this.state.employee });
            this.setState({
                employees: [...temp_employees],
                employee: { id: this.getNextId(this.state.employees), name: "", designation: "", salary: "" },
                edit: false
            });
        } else {
            const employees = [...this.state.employees, { ...this.state.employee }];
            this.setState({ employees }, () => {
                this.setState({ employee: { id: this.getNextId(this.state.employees), name: "", designation: "", salary: "" } });
            });
        }
    }

    getNextId(employees: Employee[]) {
        return employees.length ? employees[employees.length - 1].id + 1 : 1;
    }

    selectEmployee(item: Employee, allowEdit: boolean) {
        this.setState({ employee: { ...item }, edit: allowEdit });
    }

    showDeleteConfirmModal(employeeId: number) {
        this.setState({
            showDeleteConfirmModal: true,
            employeeToDeleteId: employeeId
        });
    }

    hideDeleteConfirmModal() {
        this.setState({
            showDeleteConfirmModal: false,
            employeeToDeleteId: 0
        });
    }

    confirmDelete() {
        const { employeeToDeleteId } = this.state;

        this.setState({
            employees: [...this.state.employees.filter(item => item.id !== employeeToDeleteId)]
        }, () => {
            this.setState({ employee: { id: this.getNextId(this.state.employees), name: "", designation: "", salary: "" } });
            this.hideDeleteConfirmModal();
        });
    }

    public render() {
        const items: Item[] = convertEmployeesToItems(this.state.employees);
        return (
            <div className='mt-3'>
                <FormComponent
                    employee={this.state.employee}
                    changeEmployee={this.changeEmployee}
                    saveEmployee={this.saveEmployee} />
                <hr />
                <DataTable items={items}
                    onSelect={(item: Item, isEdit: boolean) => {
                        const employee = convertItemToEmployee(item);
                        this.selectEmployee(employee, isEdit);
                    }}
                    onDelete={(id: number) => {
                        this.showDeleteConfirmModal(id);
                    }}>
                    <h5 className="text-primary text-uppercase font-weight-bold">Employees Table</h5>
                </DataTable>

                {this.state.showDeleteConfirmModal && (
                    <ConfirmModal show={this.state.showDeleteConfirmModal}
                        title="Confirm Delete"
                        message="Are you sure you want to delete this employee?"
                        handleYes={this.confirmDelete}
                        handleNo={this.hideDeleteConfirmModal} />
                )}
            </div>
        );
    }
}

export default CrudAssignment;
