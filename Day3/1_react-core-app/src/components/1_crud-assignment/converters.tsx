import { Item } from '../common/DataTable';
import { Employee } from './Employee';

export const convertEmployeesToItems = (employees: Employee[]): Item[] => {
    return employees.map((employee) => {
        return {
            id: employee.id,
            name: employee.name,
            designation: employee.designation,
            salary: employee.salary
        };
    });
};
export const convertItemToEmployee = (item: Item): Employee => {
    return {
        id: item.id as number,
        name: item.name as string,
        designation: item.designation as string,
        salary: item.salary as string,
    };
};
