import { Post } from "../components/4_ajax/post-interface";

const url: string | undefined = process.env.REACT_APP_POST_API_URL;

export const postApiClient = {
    getAllPosts: function (): Promise<Post[]> {
        return new Promise<Post[]>((resolve, reject) => {
            // Ajax call to get all posts
            fetch(url!).then(response => {
                response.json().then(posts => {
                    resolve(posts as Post[]);
                }).catch(error => {
                    reject("Parsing Error...");
                });
            }).catch(error => {
                reject("Communication Error...");
            });
        });
    },

    getAllPostsAsync: async function () {
        try {
            const response = await fetch(url!);
            const posts = await response.json() as Post[];
            return posts;
        } catch(error) {
            throw new Error("Communication Error...");
        }
    }
}