/* eslint-disable */
import React from 'react';
import EventComponent from '../1_synthetic-events/EventComponent';
import CounterAssignment from '../2_assignment/CounterAssignment';
import ControlledVsUncontrolled from '../3_controlled-vs-uncontrolled/ControlledVsUncontrolled';
import CalculatorAssignment from '../4_assignment/CalculatorAssignment';
import ListRoot from '../5_working-with-arrays/ListComponent';
import WithoutContext from '../6_context-api/WithoutContext';
import WithContext from '../6_context-api/WithContext';
import MultiContext from '../6_context-api/MultiContext';
import SiblingCommunication from '../7_communication-using-context/SiblingCommunication';

const RootComponent: React.FC = () => {
    return (
        <div className='container'>
            {/* <EventComponent /> */}
            {/* <CounterAssignment /> */}
            {/* <ControlledVsUncontrolled /> */}
            {/* <CalculatorAssignment /> */}
            {/* <ListRoot /> */}

            {/* <WithoutContext /> */}
            {/* <WithContext /> */}
            {/* <MultiContext /> */}

            <SiblingCommunication />
        </div>
    );
};

export default RootComponent;