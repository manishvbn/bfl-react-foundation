import React, { Component } from 'react';
import DataTable from '../common/DataTable';

interface Item {
    [key: string]: string | number;
}

interface ComponentTwoProps {
    data: Item[];
}

class ComponentTwo extends Component<ComponentTwoProps> {
    render() {
        return (
            <div>
                <DataTable items={this.props.data}>
                    <h4 className="text-info text-uppercase font-weight-bold">Employees Table</h4>
                </DataTable>
            </div>
        );
    }
}

interface ComponentOneProps {
    data: Item[];
}

class ComponentOne extends Component<ComponentOneProps> {
    render() {
        return (
            <div>
                <ComponentTwo data={this.props.data} />
            </div>
        );
    }
}

interface WithoutContextState {
    employees: Item[];
}

class WithoutContext extends Component<{}, WithoutContextState>{
    state = {
        employees: [
            { id: 1, name: "Manish" },
            { id: 2, name: "Abhijeet" },
            { id: 3, name: "Ramakant" },
            { id: 4, name: "Subodh" },
            { id: 5, name: "Abhishek" }
        ]
    };

    render() {
        return (
            <div>
                <h2 className="text-primary text-uppercase text-center mt-5 mb-5">Without Context API</h2>

                <ComponentOne data={this.state.employees} />
            </div>
        );
    }
}

export default WithoutContext;
