import * as React from 'react';

export interface ICounterProps {
    interval?: number
}

interface ICounterState {
    flag: boolean;
    count: number;
}

export class Counter extends React.Component<ICounterProps, ICounterState> {
    private clickCount: number = 0;

    constructor(props: ICounterProps) {
        super(props);
        this.state = {
            flag: false,
            count: 0
        };
    }

    manageClickCount(): void {
        this.clickCount++;
        if (this.clickCount > 9) {
            this.setState({ flag: true });
        }
    }

    inc(): void {
        if (this.props.interval)
            this.setState({ count: this.state.count + this.props.interval }, () => {
                this.manageClickCount();
            });
    }

    dec(): void {
        if (this.props.interval)
            this.setState({ count: this.state.count - this.props.interval }, () => {
                this.manageClickCount();
            });
    }

    public render() {
        return (
            <>
                <div className="text-center">
                    <h3 className="text-info">Counter Component</h3>
                </div>
                <div className="d-grid gap-2 mx-auto col-6">
                    <input type="text" className="form-control form-control-lg" value={this.state.count} readOnly />
                    <button className="btn btn-info" disabled={this.state.flag} onClick={this.inc.bind(this)}>
                        <span className='fs-4'>+</span>
                    </button>
                    <button className="btn btn-info" disabled={this.state.flag} onClick={this.dec.bind(this)}>
                        <span className='fs-4'>-</span>
                    </button>
                    <button className="btn btn-secondary" disabled={!this.state.flag} onClick={this.reset.bind(this)}>
                        <span className='fs-4'>Reset</span>
                    </button>
                </div>
            </>
        );
    }

    static defaultProps: ICounterProps = {
        interval: 1
    }

    reset(): void {
        this.clickCount = 0;
        this.setState({ count: 0, flag: false });
    }
}

class CounterAssignment extends React.Component {
    render() {
        return (
            <div>
                <Counter />
                <hr />
                <Counter interval={10} />
            </div>
        );
    }
}

export default CounterAssignment;
