import React from 'react';

interface Item {
    [key: string]: string | number;
}

interface DataTableProps {
    items: Item[];
    children?: React.ReactNode;
}

interface ThProps {
    item: Item;
}

const Th: React.FC<ThProps> = ({ item }) => {
    const allHeads = [...Object.keys(item), '', 'actions', ''];

    const ths = allHeads.map((head, index) => {
        return <th key={index}>{(head === '' ? '' : head.toUpperCase())}</th>;
    });

    return <tr>{ths}</tr>;
}

interface TrProps {
    item: Item;
}

const Tr: React.FC<TrProps> = ({ item }) => {
    const allValues = [...Object.values(item),
    <a href="/#" className="text-primary">Details</a>,
    <a href="/#" className="text-warning">Edit</a>,
    <a href="/#" className="text-danger">Delete</a>];

    const tds = allValues.map((item, index) => {
        return <td key={index}>{item}</td>;
    });

    return <tr>{tds}</tr>;
};

const DataTable: React.FC<DataTableProps> = ({ items }) => {
    let headers: JSX.Element | null = null;
    let trs: JSX.Element[] | null = null;

    if (items && items.length) {
        const [item] = items;
        headers = <Th item={item} />;
        trs = items.map((item, index) => {
            return <Tr item={item} key={index} />;
        });
    }
    return (
        <table className="table table-striped">
            <thead>{headers}</thead>
            <tbody>{trs}</tbody>
        </table>
    );
}

export default DataTable;