import * as React from 'react';
import DataTable from '../common/DataTable';

interface ListItemProps {
    item: { id: number; name: string };
}

const ListItem: React.FC<ListItemProps> = ({ item }) => (
    <li className='list-group-item'>{item.name}</li>
);

interface ListComponentProps {
    items: { id: number; name: string }[];
    children: React.ReactNode;
}

const ListComponent: React.FC<ListComponentProps> = (props) => {
    const l_items = props.items.map((item) => (
        <ListItem item={item} key={item.id} />
    ));

    return (
        <>
            {props.children}
            <hr />
            <ul className="list-group">
                {l_items}
            </ul>
        </>
    );
};

interface IListRootProps {
}

// interface Employee {
//     id: number;
//     name: string;
// }

// interface Post {
//     userId: number;
//     id: number;
//     title: string;
//     body: string;
// }

// interface IListRootState {
//     employees: Employee[];
//     posts: Post[];
// }

interface Item {
    [key: string]: string | number;
}

interface IListRootState {
    employees: Item[];
    posts: Item[];
}

export default class ListRoot extends React.Component<IListRootProps, IListRootState> {
    constructor(props: {}) {
        super(props);
        this.state = {
            employees: [
                { id: 1, name: "Manish" },
                { id: 2, name: "Abhijeet" },
                { id: 3, name: "Ramakant" },
                { id: 4, name: "Subodh" },
                { id: 5, name: "Abhishek" }
            ],
            posts: [
                {
                    userId: 1,
                    id: 1,
                    title: "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
                    body: "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
                },
                {
                    userId: 1,
                    id: 2,
                    title: "qui est esse",
                    body: "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla"
                }
            ]
        }
    }

    public render() {
        return (
            <div>
                {/* <ListComponent items={this.state.employees}>
                    <h4 className='text-info'>Employees Data</h4>
                </ListComponent>

                <ListComponent items={this.state.posts}>
                    <h4 className='text-info'>Posts Data</h4>
                </ListComponent> */}

                <DataTable items={this.state.employees}>
                    <h4 className='text-info'>Employees Table</h4>
                </DataTable>

                <div className="mt-5"></div>

                <DataTable items={this.state.posts}>
                    <h4 className='text-info'>Posts Table</h4>
                </DataTable>
            </div>
        );
    }
}
