import React, { MouseEvent } from 'react';

export interface IEventComponentProps {
}

export default class EventComponent extends React.Component<IEventComponentProps> {
    constructor(props: IEventComponentProps) {
        super(props);
        this.handleClick3 = this.handleClick3.bind(this);
    }

    handleClick1(e: MouseEvent<HTMLAnchorElement>) {
        console.log("event:", e);
        console.log("this: ", this);
        e.preventDefault();
    }

    handleClick2(e: MouseEvent<HTMLAnchorElement>) {
        console.log("event:", e);
        console.log("this: ", this);
        e.preventDefault();
    }

    handleClick3(e: MouseEvent<HTMLAnchorElement>) {
        console.log("event:", e);
        console.log("this: ", this);
        e.preventDefault();
    }

    handleClick4(name: string, e: MouseEvent<HTMLAnchorElement>) {
        console.log("name: ", name);
        console.log("event:", e);
        console.log("this: ", this);
        e.preventDefault();
    }

    public render() {
        return (
            <div className='text-center'>
                <h2 className="text-primary">Synthetic Event Object</h2>
                <div className="mt-2">
                    <a href="https://www.google.com" onClick={this.handleClick1}>Click One</a>
                </div>
                <div className="mt-2">
                    <a href="https://www.google.com" onClick={this.handleClick2.bind(this)}>Click Two</a>
                </div>
                <div className="mt-2">
                    <a href="https://www.google.com" onClick={this.handleClick3}>Click Three</a>
                </div>
                <div className="mt-2">
                    <a href="https://www.google.com" onClick={this.handleClick4.bind(this, "BFL")}>Click Four</a>
                </div>
                <div className="mt-2">
                    <a href="https://www.google.com" onClick={
                        function (this: EventComponent, e: MouseEvent<HTMLAnchorElement>) {
                            console.log("event:", e);
                            console.log("this: ", this);
                            e.preventDefault();
                        }
                    }>
                        Click Five - Anonymous Function
                    </a>
                </div>
                <div className="mt-2">
                    <a href="https://www.google.com" onClick={
                        (function (this: EventComponent, e: MouseEvent<HTMLAnchorElement>) {
                            console.log("event:", e);
                            console.log("this: ", this);
                            e.preventDefault();
                        }).bind(this)
                    }>
                        Click Six - Anonymous Function
                    </a>
                </div>
                <div className="mt-2">
                    <a href="https://www.google.com" onClick={
                        (e: MouseEvent<HTMLAnchorElement>) => {
                            console.log("event:", e);
                            console.log("this: ", this);
                            e.preventDefault();
                        }
                    }>
                        Click Seven - Lambda
                    </a>
                </div>
                <div className="mt-2">
                    <a href="https://www.google.com" onClick={
                        (e: MouseEvent<HTMLAnchorElement>) => {
                            this.handleClick4("Google", e);
                        }
                    }>
                        Click Eight - Lambda
                    </a>
                </div>
            </div>
        );
    }
}

// https://codepen.io/manishvbn/pen/mEKvxY