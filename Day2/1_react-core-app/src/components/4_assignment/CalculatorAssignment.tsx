import * as React from 'react';
import { ChangeEvent, FormEvent, RefObject } from 'react';
import TextInput from '../common/TextInput';

export interface ICalculatorProps {
}

interface ICalculatorState {
    data: {
        t1: number;
        t2: number;
    };
    result: number;
}

class CalculatorOne extends React.Component<ICalculatorProps, ICalculatorState> {
    private t1: RefObject<HTMLInputElement>;
    private t2: RefObject<HTMLInputElement>;

    constructor(props: {}) {
        super(props);
        this.t1 = React.createRef();
        this.t2 = React.createRef();
        this.state = { data: { t1: 0, t2: 0 }, result: 0 };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.reset = this.reset.bind(this);
    }

    handleSubmit(e: FormEvent) {
        e.preventDefault();
        if (this.t1.current && this.t2.current) {
            const num1 = parseInt(this.t1.current.value) || 0;
            const num2 = parseInt(this.t2.current.value) || 0;
            this.setState({ result: num1 + num2 });
        }
    }

    reset(e: FormEvent) {
        this.setState({ data: { t1: 0, t2: 0 }, result: 0 }, () => {
            if (this.t1.current && this.t2.current) {
                this.t1.current.value = "0";
                this.t2.current.value = "0";
            }
        });

        e.preventDefault();
    }

    render() {
        return (
            <div className="row">
                <div className="col-sm-6 offset-sm-3">
                    <form className="justify-content-center">
                        <fieldset>
                            <legend className="text-center">Calculator One - Uncontrolled</legend>
                            <div className="form-group mb-1">
                                <label className="mb-0" htmlFor="t1">Number One</label>
                                <input type="text" className="form-control" id="t1"
                                    ref={this.t1}
                                    defaultValue={this.state.data.t1} />
                            </div>
                            <div className="form-group mb-1">
                                <label className="mb-0" htmlFor="t2">Number Two</label>
                                <input type="text" className="form-control" id="t2"
                                    ref={this.t2}
                                    defaultValue={this.state.data.t2} />
                            </div>
                            <div className="form-group mb-2 mt-2">
                                <h3>Result: {this.state.result}</h3>
                            </div>
                            <div className="d-grid gap-2 mx-auto">
                                <button type="submit" className="btn btn-success" onClick={this.handleSubmit}>Add</button>
                                <button type="reset" className="btn btn-primary" onClick={this.reset}>Reset</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        );
    }
}

class CalculatorTwo extends React.Component<ICalculatorProps, ICalculatorState> {
    constructor(props: {}) {
        super(props);
        this.state = { data: { t1: 0, t2: 0 }, result: 0 };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.reset = this.reset.bind(this);
        this.handleChange1 = this.handleChange1.bind(this);
        this.handleChange2 = this.handleChange2.bind(this);
    }

    handleChange1(e: ChangeEvent<HTMLInputElement>) {
        this.setState((prevState) => ({
            data: { ...prevState.data, t1: Number(e.target.value) },
        }));
    }

    handleChange2(e: ChangeEvent<HTMLInputElement>) {
        this.setState((prevState) => ({
            data: { ...prevState.data, t2: Number(e.target.value) },
        }));
    }

    handleSubmit(e: FormEvent) {
        e.preventDefault();
        const { t1, t2 } = this.state.data;
        this.setState({ result: t1 + t2 });
    }

    reset(e: FormEvent) {
        this.setState({ data: { t1: 0, t2: 0 }, result: 0 });
        e.preventDefault();
    }

    render() {
        return (
            <div className="row">
                <div className="col-sm-6 offset-sm-3">
                    <form className="justify-content-center">
                        <fieldset>
                            <legend className="text-center">Calculator Two - Controlled</legend>
                            <div className="form-group mb-1">
                                <label className="mb-0" htmlFor="t1">Number One</label>
                                <input type="text" className="form-control" id="t1"
                                    value={this.state.data.t1} onChange={this.handleChange1} />
                            </div>
                            <div className="form-group mb-1">
                                <label className="mb-0" htmlFor="t2">Number Two</label>
                                <input type="text" className="form-control" id="t2"
                                    value={this.state.data.t2} onChange={this.handleChange2} />
                            </div>
                            <div className="form-group mb-2 mt-2">
                                <h3>Result: {this.state.result}</h3>
                            </div>
                            <div className="d-grid gap-2 mx-auto">
                                <button type="submit" className="btn btn-success" onClick={this.handleSubmit}>Add</button>
                                <button type="reset" className="btn btn-primary" onClick={this.reset}>Reset</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        );
    }
}

class CalculatorThree extends React.Component<ICalculatorProps, ICalculatorState> {
    constructor(props: {}) {
        super(props);
        this.state = { data: { t1: 0, t2: 0 }, result: 0 };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.reset = this.reset.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e: ChangeEvent<HTMLInputElement>) {
        const field = e.target.id;
        var obj = { ...this.state.data };
        obj[field as keyof typeof obj] = Number(e.target.value);
        this.setState({ data: obj });
    }

    handleSubmit(e: FormEvent) {
        e.preventDefault();
        const { t1, t2 } = this.state.data;
        this.setState({ result: t1 + t2 });
    }

    reset(e: FormEvent) {
        this.setState({ data: { t1: 0, t2: 0 }, result: 0 });
        e.preventDefault();
    }

    render() {
        return (
            <div className="row">
                <div className="col-sm-6 offset-sm-3">
                    <form className="justify-content-center">
                        <fieldset>
                            <legend className="text-center">Calculator Three - Controlled - Single Change Handler</legend>
                            <div className="form-group mb-1">
                                <label className="mb-0" htmlFor="t1">Number One</label>
                                <input type="text" className="form-control" id="t1"
                                    value={this.state.data.t1} onChange={this.handleChange} />
                            </div>
                            <div className="form-group mb-1">
                                <label className="mb-0" htmlFor="t2">Number Two</label>
                                <input type="text" className="form-control" id="t2"
                                    value={this.state.data.t2} onChange={this.handleChange} />
                            </div>
                            <div className="form-group mb-2 mt-2">
                                <h3>Result: {this.state.result}</h3>
                            </div>
                            <div className="d-grid gap-2 mx-auto">
                                <button type="submit" className="btn btn-success" onClick={this.handleSubmit}>Add</button>
                                <button type="reset" className="btn btn-primary" onClick={this.reset}>Reset</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        );
    }
}

// interface ICalculatorThreeState {
//     data: {
//         t1: number;
//         t2: number;
//     };
//     result: number;
//     errors: {
//         t1: string;
//         t2: string;
//     };
// }

// class CalculatorThree extends React.Component<ICalculatorProps, ICalculatorThreeState> {
//     constructor(props: {}) {
//         super(props);
//         this.state = { data: { t1: 0, t2: 0 }, result: 0, errors: { t1: '', t2: '' } };
//         this.handleSubmit = this.handleSubmit.bind(this);
//         this.reset = this.reset.bind(this);
//         this.handleChange = this.handleChange.bind(this);
//     }

//     handleChange(e: ChangeEvent<HTMLInputElement>) {
//         const field = e.target.id;
//         const value = e.target.value;

//         const obj = { ...this.state.data };
//         const errors = { ...this.state.errors };

//         try {
//             const parsedValue = Number(value);
//             if (isNaN(parsedValue)) {
//                 errors[field as keyof typeof errors] = 'Please enter a valid number';
//             } else {
//                 obj[field as keyof typeof obj] = parsedValue;
//                 errors[field as keyof typeof errors] = '';
//             }
//         } catch (error) {
//             errors[field as keyof typeof errors] = 'Please enter a valid number';
//         }

//         this.setState({ data: obj, errors });
//     }

//     handleSubmit(e: FormEvent) {
//         e.preventDefault();
//         const { t1, t2 } = this.state.data;
//         this.setState({ result: t1 + t2 });
//     }

//     reset(e: FormEvent) {
//         this.setState({ data: { t1: 0, t2: 0 }, result: 0, errors: { t1: '', t2: '' } });
//         e.preventDefault();
//     }

//     render() {
//         const isT1Invalid = !!this.state.errors.t1;
//         const isT2Invalid = !!this.state.errors.t2;

//         return (
//             <div className="row">
//                 <div className="col-sm-6 offset-sm-3">
//                     <form className="justify-content-center">
//                         <fieldset>
//                             <legend className="text-center">Calculator Three - Controlled - Single Change Handler</legend>
//                             <div className="form-group mb-1">
//                                 <label className="mb-0" htmlFor="t1">Number One</label>
//                                 <input type="text"
//                                     className={`form-control ${isT1Invalid ? 'is-invalid' : ''}`}
//                                     id="t1"
//                                     value={this.state.data.t1} onChange={this.handleChange} />
//                                 {isT1Invalid && <div className="invalid-feedback">{this.state.errors.t1}</div>}
//                             </div>
//                             <div className="form-group mb-1">
//                                 <label className="mb-0" htmlFor="t2">Number Two</label>
//                                 <input type="text" className={`form-control ${isT2Invalid ? 'is-invalid' : ''}`} id="t2"
//                                     value={this.state.data.t2} onChange={this.handleChange} />
//                                 {isT2Invalid && <div className="invalid-feedback">{this.state.errors.t2}</div>}
//                             </div>
//                             <div className="form-group mb-2 mt-2">
//                                 <h3>Result: {this.state.result}</h3>
//                             </div>
//                             <div className="d-grid gap-2 mx-auto">
//                                 <button type="submit" className="btn btn-success" onClick={this.handleSubmit}>Add</button>
//                                 <button type="reset" className="btn btn-primary" onClick={this.reset}>Reset</button>
//                             </div>
//                         </fieldset>
//                     </form>
//                 </div>
//             </div>
//         );
//     }
// }

class CalculatorFour extends React.Component<ICalculatorProps, ICalculatorState> {
    constructor(props: {}) {
        super(props);
        this.state = { data: { t1: 0, t2: 0 }, result: 0 };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.reset = this.reset.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e: ChangeEvent<HTMLInputElement>) {
        const field = e.target.id;
        var obj = { ...this.state.data };
        obj[field as keyof typeof obj] = Number(e.target.value);
        this.setState({ data: obj });
    }

    handleSubmit(e: FormEvent) {
        e.preventDefault();
        const { t1, t2 } = this.state.data;
        this.setState({ result: t1 + t2 });
    }

    reset(e: FormEvent) {
        this.setState({ data: { t1: 0, t2: 0 }, result: 0 });
        e.preventDefault();
    }

    render() {
        return (
            <div className="row">
                <div className="col-sm-6 offset-sm-3">
                    <form className="justify-content-center">
                        <fieldset>
                            <legend className="text-center">Calculator Four - Composite UI</legend>

                            <TextInput name="t1" label="Number One" placeholder="Enter Number One"
                                value={this.state.data.t1} onChange={this.handleChange} />

                            <TextInput name="t2" label="Number Two" placeholder="Enter Number Two"
                                value={this.state.data.t2} onChange={this.handleChange} />

                            <div className="form-group mb-2 mt-2">
                                <h3>Result: {this.state.result}</h3>
                            </div>
                            <div className="d-grid gap-2 mx-auto">
                                <button type="submit" className="btn btn-success" onClick={this.handleSubmit}>Add</button>
                                <button type="reset" className="btn btn-primary" onClick={this.reset}>Reset</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        );
    }
}

const CalculatorAssignment: React.FC = () => {
    return (
        <div>
            {/* <CalculatorOne /> */}
            {/* <CalculatorTwo /> */}
            {/* <CalculatorThree /> */}
            <CalculatorFour />
        </div>
    );
}

export default CalculatorAssignment;