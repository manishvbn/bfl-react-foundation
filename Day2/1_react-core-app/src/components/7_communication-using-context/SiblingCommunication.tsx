import * as React from 'react';
import Counter from './Counter';
import CounterSibling from './CounterSibling';
import CounterContextProvider from './CounterContext';

export default class SiblingCommunication extends React.Component {
    public render() {
        return (
            <div className='mt-5'>
                <CounterContextProvider>
                    <Counter />
                    <hr />
                    <CounterSibling />
                </CounterContextProvider>
            </div>
        );
    }
}
