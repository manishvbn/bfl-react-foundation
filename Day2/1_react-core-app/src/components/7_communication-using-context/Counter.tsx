import * as React from 'react';
import { CounterContext } from './CounterContext';

export default class Counter extends React.Component {
    static contextType = CounterContext;
    context!: React.ContextType<typeof CounterContext>;

    constructor(props: {}) {
        super(props);
        this._inc = this._inc.bind(this);
        this._dec = this._dec.bind(this);
    }

    _inc(e: React.MouseEvent<HTMLButtonElement>) {
        const { count, setCount } = this.context!;
        if (setCount) {
            setCount(count + 1);
        }
    }

    _dec(e: React.MouseEvent<HTMLButtonElement>) {
        const { count, setCount } = this.context!;
        if (setCount) {
            setCount(count - 1);
        }
    }

    public render() {
        return (
            <>
                <div className="text-center">
                    <h3 className="text-info">Counter Component</h3>
                </div>
                <div className="d-grid gap-2 mx-auto col-6">
                    <h2 className="text-info text-center">
                        Current Count is: {this.context!.count.toString()}
                    </h2>
                    <button className="btn btn-info"
                        onClick={this._inc}>
                        <span className='fs-4'>+</span>
                    </button>
                    <button className="btn btn-info"
                        onClick={this._dec}>
                        <span className='fs-4'>-</span>
                    </button>
                </div>
            </>
        );
    }
}
