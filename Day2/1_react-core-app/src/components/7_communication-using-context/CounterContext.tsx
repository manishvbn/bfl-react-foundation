import { Component, ReactNode, createContext } from "react";

interface CounterContextValue {
    count: number;
    setCount?: (newCount: number) => void;
}

export const CounterContext = createContext<CounterContextValue | undefined>(undefined);

interface ICounterContextProviderProps {
    children: ReactNode;
}

class CounterContextProvider extends Component<ICounterContextProviderProps, CounterContextValue> {
    constructor(props: ICounterContextProviderProps) {
        super(props);
        this.state = { count: 0 };
        this.setCount = this.setCount.bind(this);
    }

    setCount(newCount: number) {
        this.setState({ count: newCount });
    }

    render(): ReactNode {
        return (
            <CounterContext.Provider value={{count: this.state.count, setCount: this.setCount}}>
                {this.props.children}
            </CounterContext.Provider>
        );
    }
}

export default CounterContextProvider;