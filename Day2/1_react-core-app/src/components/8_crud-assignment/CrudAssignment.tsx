import React, { Component } from 'react';
import DataTable from '../common/DataTable';
import TextInput from '../common/TextInput';

// Define the type for the employee object
interface Employee {
    id: number;
    name: string;
    designation: string;
    salary: string;
}

// Define the component for the form
const FormComponent: React.FC = () => {
    // Handle form submission here
    const handleSubmit = (e: React.FormEvent) => {
        e.preventDefault();
        // Add your logic to handle form submission, e.g., sending data to the server
    };

    return (
        <>
            <div className='row'>
                <div className='col-sm-6 offset-sm-3'>
                    <form className='form-horizontal' autoComplete='off' onSubmit={handleSubmit}>
                        <fieldset>
                            <legend className="text-center text-secondary text-uppercase font-weight-bold">Add/Edit Employee Information</legend>
                            <hr className="mt-0" />
                            {/* Use TextInput component for input fields */}
                            <TextInput label={"Employee ID"} name={"id"} />
                            <TextInput label={"Employee Name"} name={"name"} />
                            <TextInput label={"Designation"} name={"designation"} />
                            <TextInput label={"Salary"} name={"salary"} />

                            <div className="d-grid gap-2 mx-auto mt-3">
                                <button type="submit" className="btn btn-success">Save</button>
                                <button type="reset" className="btn btn-primary">Reset</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </>
    );
};

// Define the CrudAssignment component
class CrudAssignment extends Component<{}, { employees: Employee[]; employee: Employee }> {
    constructor(props: {}) {
        super(props);
        this.state = {
            employees: [],
            employee: { id: 1, name: "", designation: "", salary: "" }
        };
    }

    public render() {
        return (
            <div className='mt-3'>
                {/* Render the FormComponent */}
                <FormComponent />
                <hr />
                <DataTable items={this.state.employees}>
                    <h5 className="text-primary text-uppercase font-weight-bold">Employees Table</h5>
                </DataTable>
            </div>
        );
    }
}

export default CrudAssignment;
